console.log("Hello Worlds!!!!!");

let number = Number(prompt("Enter number:"));
console.log("The number you provided is " + number + ".");

for (let i = number; i > 0; i--) {
	if (i <= 50) {
		console.log("The current value is at " + i + ". Terminating the loop.")
		break;
	}

	if (i % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.")	;
		continue;	
	}

	if (i % 5 === 0) {
		console.log(i);
	}
}

let myString = "supercalifragilisticexpialidocious";
console.log(myString);

let modifiedString = "";

for (let x = 0; x < myString.length; x++) {
	if (
		myString[x] == "a" ||
		myString[x] == "e" ||
		myString[x] == "i" ||
		myString[x] == "o" ||
		myString[x] == "u"
		) {
		continue
	} else {
		modifiedString += myString[x];
	}
}

console.log(modifiedString);